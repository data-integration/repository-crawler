FROM python:3.7-slim

ENV PYTHONUNBUFFERED=1

WORKDIR /code/

COPY requirements.txt .
RUN apt-get update \
    && apt-get install -y --no-install-recommends gcc linux-libc-dev libc6-dev \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir -r requirements.txt \
    && python -m spacy download en \
    && apt-get purge -y --auto-remove gcc linux-libc-dev libc6-dev

RUN useradd user \
 && chown -R user /code

USER user

COPY . .

CMD [ "python", "-m", "repository_crawler" ]
