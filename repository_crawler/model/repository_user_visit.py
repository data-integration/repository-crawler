from sqlalchemy import Column, Integer, ForeignKey, Float
from sqlalchemy.orm import relationship

from .base import Base


class RepositoryUserVisit(Base):
    __tablename__ = 'repository_user_visit'

    # Columns
    repository_id = Column(Integer, ForeignKey('repository.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    weight = Column(Float)

    # Relationships
    repository = relationship('Repository', back_populates='visitors')
    user = relationship('User', back_populates='visited_repositories')
