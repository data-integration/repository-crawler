from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey, UniqueConstraint, Boolean
from sqlalchemy.orm import relationship

from .base import Base


class Repository(Base):
    __tablename__ = 'repository'
    __table_args__ = (UniqueConstraint('external_id', 'platform_id', name='repository_platform_uc'),)

    # Columns
    id = Column(Integer, primary_key=True)
    external_id = Column(String, nullable=False)
    platform_id = Column(Integer, ForeignKey('platform.id'), nullable=False)
    name = Column(String, nullable=False)
    description = Column(Text)
    owner_id = Column(Integer, ForeignKey('user.id'))
    created = Column(DateTime)
    last_updated = Column(DateTime)
    readme_url = Column(String)
    language_id = Column(Integer, ForeignKey('language.id'))
    details_crawled = Column(Boolean, default=False)

    # Relationships
    platform = relationship("Platform", foreign_keys=[platform_id])
    owner = relationship("User", foreign_keys=[owner_id])
    language = relationship("Language", foreign_keys=[language_id])
    topics = relationship('RepositoryTopic', back_populates="repository", cascade="all, delete-orphan")
    visitors = relationship('RepositoryUserVisit', back_populates='repository', cascade="all, delete-orphan")
