from repository_crawler import Topic
from .session_manager import SessionManager


class TopicManager(SessionManager):

    def topics_exist(self):
        return self.session.query(Topic).first() is not None

    def get_all(self):
        return self.session.query(Topic).all()

    def get_by_name(self, name):
        return self.session.query(Topic).filter_by(name=name).first()

    def add_topic_if_not_exist(self, topic: Topic):
        old_topic = self.session.query(Topic).filter_by(name=topic.name).first()
        if old_topic:
            return old_topic
        else:
            self.session.add(topic)
            self.session.commit()
            return topic

    def commit(self):
        self.session.commit()
