from repository_crawler import User, Repository
from .session_manager import SessionManager


class UserManager(SessionManager):

    def add_user_if_not_exist(self, user: User):
        old_user = self.session.query(User).filter_by(external_id=user.external_id,
                                                      platform_id=user.platform_id).first()
        if old_user:
            return old_user
        else:
            self.session.add(user)
            self.session.commit()
            return user

    def get_owner_by_repo(self, repo: Repository):
        return self.session.query(User).get(repo.owner_id)

    def commit(self):
        self.session.commit()
