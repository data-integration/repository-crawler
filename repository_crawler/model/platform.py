from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base


class Platform(Base):
    __tablename__ = 'platform'

    # Columns
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

    # Relationships
    repositories = relationship('Repository', back_populates='platform')
    users = relationship('User', back_populates='platform')
