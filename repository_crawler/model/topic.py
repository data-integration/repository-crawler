from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from .base import Base


class Topic(Base):
    __tablename__ = 'topic'

    # Columns
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    featured = Column(Boolean, default=False)
    curated = Column(Boolean, default=False)

    # Relationships
    repositories = relationship('RepositoryTopic', back_populates="topic")
