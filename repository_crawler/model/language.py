from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base


class Language(Base):
    __tablename__ = 'language'

    # Columns
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

    # Relationships
    repositories = relationship('Repository', back_populates='language', foreign_keys='Repository.language_id')
