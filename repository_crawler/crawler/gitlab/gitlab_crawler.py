import json
import logging
import urllib.parse

from dateutil import parser

import repository_crawler.config as config
from repository_crawler import User
from repository_crawler.crawler.crawler_utils import check_rate_limit
from repository_crawler.crawler.crawler_utils import get_next_url
from repository_crawler.crawler.crawler_utils import get_request
from repository_crawler.model.repository_manager import RepositoryManager
from repository_crawler.model.setting_manager import SettingManager
from repository_crawler.model.user_manager import UserManager

LAST_PAGE_CRAWLED_KEY = "gitlab.last_page_crawled"


class GitlabCrawler:
    def __init__(self, run_event, per_page=50):
        self.repository_manager = RepositoryManager()
        self.user_manager = UserManager()
        self.setting_manager = SettingManager()
        self.page = int(self.setting_manager.get_setting(LAST_PAGE_CRAWLED_KEY, 0)) + 1
        self.run_event = run_event
        self.per_page = per_page

    def crawl(self):
        url = config.GITLAB_API_PROJECTS
        parameters = self._get_parameters()

        while self.run_event.is_set():
            # Make request
            # This request is not authenticated due to a bug in the GitLab API resulting in internal server error
            # responses when requesting pages after 2044 with authentication
            response = get_request(url, parameters)

            if response is None:
                logging.warning(f"Page {self.page} could not be crawled - falling back to one-by-one crawling")
                response = self._crawl_until_next_page()

            # Parse projects
            for project in json.loads(response.content):
                self._parse_project(project)

            # Update pages done
            pages_done = response.headers["X-Page"]
            logging.info(f"Successfully parsed page {pages_done}")
            self.setting_manager.update_setting(LAST_PAGE_CRAWLED_KEY, pages_done)
            self.page = int(pages_done) + 1

            # Determine next url
            url = get_next_url(response)
            parameters = {}
            if url is None:
                logging.info(f"Finished crawling")
                break

            # Check rate limit
            check_rate_limit(response, remaining_key="RateLimit-Remaining", reset_key="RateLimit-Reset")

    def _crawl_until_next_page(self):
        url = config.GITLAB_API_PROJECTS

        next_successful_response = get_request(url, self._get_parameters(page=self.page + 1))
        next_successful_external_id = json.loads(next_successful_response.content)[0]["id"]

        logging.info(f"Crawling projects one-by-one until external ID {next_successful_external_id}")

        page = (self.page - 1) * self.per_page

        while True:
            page += 1

            response = get_request(url, self._get_parameters(per_page=1, page=page))
            if response is None:
                logging.warning(f"One-by-one crawling of page {page} was not successful - skipping")
                continue

            project = json.loads(response.content)[0]

            if project["id"] >= next_successful_external_id:
                break
            else:
                self._parse_project(json.loads(response.content)[0])

        return next_successful_response

    def _get_parameters(self, page=None, per_page=None):
        if page is None:
            page = self.page
        if per_page is None:
            per_page = self.per_page

        return {
            "membership": "false",
            "order_by": "id",
            "owned": "false",
            "page": page,
            "per_page": per_page,
            "repository_checksum_failed": "false",
            "simple": "true",
            "sort": "asc",
            "starred": "false",
            "statistics": "false",
            "wiki_checksum_failed": "false",
            "with_custom_attributes": "false",
            "with_issues_enabled": "false",
            "with_merge_requests_enabled": "false"
        }

    def _parse_project(self, project):
        logging.debug(f"Parsing project {project}")

        if self.repository_manager.repository_exist(external_id=str(project["id"]),
                                                    platform_id=config.GITLAB_ID):
            logging.info(f"Project with ID {project['id']} already exists - skipping")
            return

        if 'star_count' not in project:
            return

        if project['star_count'] < config.WATCHERS_THRESHOLD:
            return

        if 'last_activity_at' not in project:
            return

        if project['last_activity_at'] is None:
            return

        if parser.parse(project['last_activity_at']) < config.LAST_UPDATED_THRESHOLD:
            return

        repository = self.repository_manager.get_or_create_repository(external_id=str(project["id"]),
                                                                      platform_id=config.GITLAB_ID)
        repository.name = project["name"]
        repository.description = project["description"]
        repository.created = project['created_at']
        repository.last_updated = project['last_activity_at']
        if ('readme_url' in project) and ('default_branch' in project):
            if (not project['readme_url'] is None) and (not project['default_branch'] is None):
                branch = project['default_branch']
                filepath = project['readme_url'].split(f"/blob/{branch}/")[1]
                url = config.GITLAB_API_PROJECT_FILES % (project["id"], urllib.parse.quote(filepath, safe=''), branch)
                repository.readme_url = url

        if repository.readme_url is not None:
            url = config.GITLAB_API_PROJECTS_DETAILS % repository.external_id
            response = get_request(url, token=config.GITLAB_API_KEY)
            project_details = json.loads(response.content)

            if project_details['empty_repo']:
                return

            owner = None

            if 'owner' in project_details:
                owner = User(
                    external_id=str(project_details['owner']['id']),
                    platform_id=config.GITLAB_ID,
                    name=project_details['owner']['username']
                )
                owner = self.user_manager.add_user_if_not_exist(owner)
                repository.owner_id = owner.id
                self.user_manager.commit()

            self.repository_manager.add_repository(repository)
            if owner is not None:
                self.repository_manager.add_owner(repository, owner)
            self.repository_manager.commit()
