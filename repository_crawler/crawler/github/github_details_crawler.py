import json
import logging
import time

import repository_crawler.config as config
from repository_crawler.crawler.details_crawler import DetailsCrawler
from repository_crawler.crawler.crawler_utils import get_request, check_rate_limit, get_next_url
from repository_crawler.model.repository import Repository
from repository_crawler.model.user import User
from repository_crawler.model.user_manager import UserManager


class GithubDetailsCrawler(DetailsCrawler):
    def __init__(self, run_event):
        super().__init__()
        self.user_manager = UserManager()
        self.run_event = run_event
        self.per_page = 100

    def crawl(self):
        while self.run_event.is_set():
            repositories = self.repository_manager.get_repositories_without_details(config.GITHUB_ID)

            if len(repositories) == 0:
                logging.info(f"Sleeping for {config.DETAILS_CRAWLER_SLEEP_TIME} seconds")
                time.sleep(config.DETAILS_CRAWLER_SLEEP_TIME)
            else:
                for repository in repositories:
                    logging.info(f"Crawling details for repository with ID {repository.id}")
                    if self._crawl_readme(repository):
                        self._crawl_stargazers(repository)
                        self._crawl_watchers(repository)
                        repository.details_crawled = True
                    else:
                        self.repository_manager.delete(repository)

                    self.repository_manager.commit()

                    if not self.run_event.is_set():
                        break

    def _crawl_readme(self, repository: Repository):
        logging.debug(f"Crawling README for repository with ID {repository.id}")

        response = get_request(repository.readme_url, token=config.GITHUB_API_KEY,
                               accept="application/vnd.github.VERSION.html")
        check_rate_limit(response, remaining_key="X-RateLimit-Remaining", reset_key="X-RateLimit-Reset")

        if response is None:
            return False

        readme = response.content.decode('utf-8')

        if len(repository.topics) > config.ASSIGNED_TOPICS_THRESHOLD:
            return self._process_readme(readme, repository, pre_rendered=True, only_check_language=True)
        else:
            return self._process_readme(readme, repository, pre_rendered=True)

    def _crawl_stargazers(self, repository: Repository):
        logging.debug(f"Crawling stargazers for repository with ID {repository.id}")
        self._crawl_users(repository, config.GITHUB_API_STARGAZERS % (repository.owner.name, repository.name), False)

    def _crawl_watchers(self, repository: Repository):
        logging.debug(f"Crawling watchers for repository with ID {repository.id}")
        self._crawl_users(repository, config.GITHUB_API_WATCHERS % (repository.owner.name, repository.name), True)

    def _crawl_users(self, repository: Repository, url, watcher):
        parameters = {'per_page': self.per_page}

        while True:
            response = get_request(url, parameters, token=config.GITHUB_API_KEY)

            for user_dict in json.loads(response.content):
                user = User(
                    external_id=str(user_dict['id']),
                    platform_id=config.GITHUB_ID,
                    name=user_dict['login']
                )

                user = self.user_manager.add_user_if_not_exist(user)
                if watcher:
                    self.repository_manager.add_watcher(repository, user)
                else:
                    self.repository_manager.add_starrer(repository, user)

            url = get_next_url(response)
            parameters = {}

            if url is None:
                break

            check_rate_limit(response, remaining_key="X-RateLimit-Remaining", reset_key="X-RateLimit-Reset")
