import json
import logging

from dateutil import parser

import repository_crawler.config as config
from repository_crawler import User
from repository_crawler.crawler.crawler_utils import check_rate_limit
from repository_crawler.crawler.crawler_utils import get_next_url
from repository_crawler.crawler.crawler_utils import get_request
from repository_crawler.model.repository_manager import RepositoryManager
from repository_crawler.model.setting_manager import SettingManager
from repository_crawler.model.topic_manager import TopicManager
from repository_crawler.model.user_manager import UserManager

LAST_STARS_COUNT_CRAWLED_KEY = "github.last_stars_count_crawled"


class GithubCrawler:
    def __init__(self, run_event, per_page=100):
        self.repository_manager = RepositoryManager()
        self.user_manager = UserManager()
        self.setting_manager = SettingManager()
        self.topic_manager = TopicManager()
        self.last_stars_count_crawled = int(self.setting_manager.get_setting(LAST_STARS_COUNT_CRAWLED_KEY, 0))
        self.run_event = run_event
        self.per_page = per_page

    def crawl(self):
        url = config.GITHUB_API_SEARCH_REPOSITORIES
        query = self._get_next_query()
        if query is None:
            logging.info("Finished crawling")
            return
        parameters = {'q': query, 'sort': 'stars', 'order': 'desc', 'per_page': self.per_page}

        while self.run_event.is_set():
            # Make request
            response = get_request(url, parameters, token=config.GITHUB_API_KEY,
                                   accept="application/vnd.github.mercy-preview+json")

            last_stars_count = self.last_stars_count_crawled

            # Parse repositories
            for repository in json.loads(response.content)['items']:
                last_stars_count = self._parse_repository(repository, last_stars_count)

            # Update last stars count crawled
            logging.info(f"Successfully parsed repositories until stars count {last_stars_count}")
            self.setting_manager.update_setting(LAST_STARS_COUNT_CRAWLED_KEY, last_stars_count)

            # Determine next url
            url = get_next_url(response)
            parameters = {}

            if url is None:
                if self.last_stars_count_crawled == last_stars_count:
                    # Reduce last_stars_count_crawled by 1 to prevent getting stuck at one value
                    # This could happen if there are more than 1000 repositories with that exact star count
                    # In that case the search query can only give us 1000 repositories due to GitHub's API limitations
                    self.last_stars_count_crawled = last_stars_count - 1
                else:
                    self.last_stars_count_crawled = last_stars_count

                url = config.GITHUB_API_SEARCH_REPOSITORIES
                query = self._get_next_query()

                if query is None:
                    logging.info("Finished crawling")
                    break

                parameters = {'q': query, 'sort': 'stars', 'order': 'desc', 'per_page': self.per_page}

                logging.info(f"Next query: {query}")

            # Check rate limit
            check_rate_limit(response, remaining_key="X-RateLimit-Remaining", reset_key="X-RateLimit-Reset")

    def _get_next_query(self):
        if self.last_stars_count_crawled == 0:
            return "stars:>1"
        else:
            min_range = self.last_stars_count_crawled // 2
            max_range = self.last_stars_count_crawled

            if min_range < config.WATCHERS_THRESHOLD:
                return None

            return f"stars:{min_range}..{max_range}"

    def _parse_repository(self, repository_dict, last_stars_count):
        logging.debug(f"Parsing project {repository_dict}")

        if 'stargazers_count' not in repository_dict:
            return last_stars_count

        if repository_dict['stargazers_count'] is None:
            return last_stars_count

        stars_count = repository_dict['stargazers_count']

        if repository_dict['size'] == 0:
            return stars_count

        if parser.parse(repository_dict['updated_at']) < config.LAST_UPDATED_THRESHOLD:
            return

        owner = User(
            external_id=str(repository_dict['owner']['id']),
            platform_id=config.GITHUB_ID,
            name=repository_dict['owner']['login']
        )

        repository = self.repository_manager.get_or_create_repository(external_id=str(repository_dict["id"]),
                                                                      platform_id=config.GITHUB_ID)

        repository.name = repository_dict["name"]
        repository.description = repository_dict["description"]
        repository.created = repository_dict['created_at'],
        repository.last_updated = repository_dict['updated_at']
        repository.readme_url = config.GITHUB_API_CONTENTS_README % (owner.name, repository.name)

        user = self.user_manager.add_user_if_not_exist(owner)
        repository.owner_id = user.id
        self.user_manager.commit()
        self.repository_manager.add_repository(repository)
        self.repository_manager.add_owner(repository, user)
        self.repository_manager.commit()

        if 'language' in repository_dict:
            if repository_dict['language'] is not None:
                self.repository_manager.add_language_to_repo(repository, repository_dict['language'])

        if 'topics' in repository_dict:
            topics = []

            # Remove topics which don't belong to the pre-queried keyword base
            for topic_name in repository_dict['topics']:
                topic = self.topic_manager.get_by_name(topic_name)
                if topic is not None:
                    topics.append(topic)

            if len(topics) > config.ASSIGNED_TOPICS_THRESHOLD:
                self.repository_manager.add_weighted_topics_to_repo(
                    repository,
                    list(zip(topics, [1 / len(topics)] * len(topics)))
                )

            self.repository_manager.commit()

        return stars_count
