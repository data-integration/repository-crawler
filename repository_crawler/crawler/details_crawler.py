import logging
import traceback
from repository_crawler.crawler.topic_extractor.topic_extractor import markdown_to_text, get_topics_for_text, \
    rst_to_text, adoc_to_text, rdoc_to_text, html_to_text, check_language
from repository_crawler.model.repository import Repository
from repository_crawler.model.repository_manager import RepositoryManager


class DetailsCrawler:
    def __init__(self):
        self.repository_manager = RepositoryManager()

    def _process_readme(self, readme, repository: Repository, pre_rendered=False, only_check_language=False):
        if len(readme) == 0:
            return False

        try:
            if pre_rendered:
                readme = html_to_text(readme)
            else:
                if (".md" in repository.readme_url) or (".markdown" in repository.readme_url):
                    readme = markdown_to_text(readme)
                elif ".rst" in repository.readme_url:
                    readme = rst_to_text(readme)
                elif ".adoc" in repository.readme_url:
                    readme = adoc_to_text(readme)
                elif ".rdoc" in repository.readme_url:
                    readme = rdoc_to_text(readme)
        except Exception:
            logging.error(f"Exception while parsing README for repository with ID {repository.id} - skipping")
            traceback.print_exc()
            return False

        text = f"{repository.description} {readme}"

        if only_check_language:
            return check_language(text)

        topics = get_topics_for_text(text)

        if len(topics) == 0:
            return False

        self.repository_manager.add_topics_to_repo(repository, topics)
        return True
