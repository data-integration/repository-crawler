import os
from dateutil import parser

# General
MODE = os.environ.get('RC_MODE')
if MODE == "DEV":
    DEFAULT_LOG_LEVEL = "DEBUG"
else:
    DEFAULT_LOG_LEVEL = "INFO"
LOGLEVEL = os.environ.get('RC_LOGLEVEL', DEFAULT_LOG_LEVEL).upper()
RATE_LIMIT_SLEEP_TIME = int(os.getenv("RC_RATE_LIMIT_SLEEP_TIME", 600))
REQUEST_ERROR_SLEEP_TIME = 15
INTERNAL_SERVER_ERROR_SLEEP_TIME = 30
DETAILS_CRAWLER_SLEEP_TIME = 60
WATCHERS_THRESHOLD = 2
LAST_UPDATED_THRESHOLD = parser.parse("2015-01-01T00:00:00.000000+00:00")
ASSIGNED_TOPICS_THRESHOLD = 0

# Crawlers
CRAWL_GITHUB = os.getenv("RC_CRAWL_GITHUB", "True") == "True"
CRAWL_GITLAB = os.getenv("RC_CRAWL_GITLAB", "True") == "True"
CRAWL_BITBUCKET = os.getenv("RC_CRAWL_BITBUCKET", "True") == "True"

# Database
DB_HOST = os.getenv("RC_DB_HOST", "localhost")
DB_PORT = int(os.getenv("RC_DB_PORT", 5432))
DB_USER = os.getenv("RC_DB_USER", "admin")
DB_PASSWORD = os.getenv("RC_DB_PASSWORD", "password")
DB_NAME = os.getenv("RC_DB_NAME", "repository-recommender")
DB_SCHEMA = os.getenv("RC_DB_SCHEMA", "public")
DB_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

# Platform IDs
GITHUB_ID = 1
GITLAB_ID = 2
BITBUCKET_ID = 3

# Weights
OWNER_WEIGHT = 1.5
WATCHER_WEIGHT = 1.0
STARRER_WEIGHT = 0.75

# API keys
GITLAB_API_KEY = os.getenv("RC_GITLAB_API_KEY")
GITHUB_API_KEY = os.getenv("RC_GITHUB_API_KEY")
BITBUCKET_API_KEY = os.getenv("RC_BITBUCKET_API_KEY")

# API Endpoints
GITLAB_API_BASE = "https://gitlab.com/api/v4"
GITLAB_API_PROJECTS = f"{GITLAB_API_BASE}/projects"
GITLAB_API_PROJECTS_DETAILS = f"{GITLAB_API_BASE}/projects/%s"
GITLAB_API_PROJECT_FILES = f"{GITLAB_API_PROJECTS}/%s/repository/files/%s?ref=%s"
GITLAB_API_PROJECT_LANGUAGES = f"{GITLAB_API_PROJECTS}/%s/languages"
GITLAB_API_PROJECT_STARRERS = f"{GITLAB_API_PROJECTS}/%s/starrers"

GITHUB_API_BASE = "https://api.github.com"
GITHUB_API_SEARCH_REPOSITORIES = f"{GITHUB_API_BASE}/search/repositories"
GITHUB_API_SEARCH_TOPICS = f"{GITHUB_API_BASE}/search/topics"
GITHUB_API_CONTENTS_README = f"{GITHUB_API_BASE}/repos/%s/%s/readme"
GITHUB_API_STARGAZERS = f"{GITHUB_API_BASE}/repos/%s/%s/stargazers"
GITHUB_API_WATCHERS = f"{GITHUB_API_BASE}/repos/%s/%s/subscribers"
